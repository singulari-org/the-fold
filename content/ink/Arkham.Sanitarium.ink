You step out of the room where you traveled into The Fold, and into the long, dilapidated hallway straight out of a horror movie.

Across the isle, you see Batman conversing in hushed whispers with Elmer Fudd. Leaning against the wall, you see Harry Potter smoking a cigarette. Walking towards you, you see a plague doctor, beckoning.

"Come, come, " he says, hurriedly. "We mustn't be late."

* "Where are we going?"
    "My office."
* [Follow along.]You follow.

- Without skipping a beat, he walks past you, continuing towards the end of the hall. 

After several minutes walking through the corridors, you reach the man's office. Upon the door is the name "Corvus D. Clemmons."

* "Clemmons... like the professor?"
    "He is my brother."
    ** "I see"
* "Clemmons... like the town in North Carolina?"
    "What? No."

- "Please come inside. We have much to discuss."

Closing the door behind you, Corvus beckons to the chair across his desk. Taking a seat, you eye him suspiciously. The mask he wears is unnerving, as is his demeanor. 

"You know, nobody else has ever escaped The Fold without the help of my brother's reversing agent. You are the very first. Tell me... how did you do it?"

* "I don't know."
* "I was eaten by a bear."
    "The bear has nothing to do with it," he sighed.
    
- "You died. Normally, a person will resurrect inside of The Fold. Instead, you woke up."

He tapped his nose thoughtfully. 

"Perhaps you are a new elder god. Perhaps the Crawling Chaos is unable to keep you under his control."

"Or perhaps it was luck. You know, we're going to have to try this again. You're going to need to go back into The Fold."

* "And what if I don't want to?"
* [Shudder at the thought.]You shudder.

- "I'm not going to force you. But Arkham Sanitarium is a small place, and the surrounding grounds are dangerous. You won't want to stay here for long."

"Let me know when you are ready. We will send you back in for another test."