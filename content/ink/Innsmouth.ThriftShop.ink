Nestled at the edge of town is a small, run-down shop. It could easily be overlooked, if a person didn't know what they were looking for.

Stepping inside, you meet Dave, a Deep One. He is playing a small ukelele. It is several moments before he notices you've walked inside.

"Oh! Just blowing off a little steam. My name's Dave. Pleased to meet your acquaintance. Would you like to purchase anything?"

*"I'm just browsing."
*"Stick 'em up! This is a robbery!"
    "Bwahahahaha," Dave reacts. "There's nothing of value in here."
    **"Oh," you say, dejected.

- "Okay. Well, let me know if you need anything." 