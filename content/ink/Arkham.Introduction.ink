VAR playerName = "User"

-> Section1

= Section1
You awake, blinking as your eyes come into focus under the cold florescent lights.

You are strapped securely to your chair. Despite struggling, you cannot escape. Your heart begins to race. Your palms begin to sweat. You do not remember how you got here.

After a few moments, a large television turns on. Upon it is a man you do remember; he is Professor Clemmons, the doctor.

"Welcome back, {playerName}, I see that you are doing splentastically."

* [You blink, confused] 
* "I don't understand." 

- "Did you enjoy your time in The Fold?"

* You remember now. You are a test subject in the professor’s experiment. You have donated your brain to science. 

- "And you may have it back," the professor stated. "I am done with your mind for now. My my, you did not last very long."

* You remember more. You were a witness to your own birth - and death: 
    -> Flashback

= Flashback
An arm breaks through the surface of a shallow grave. Then, another. The shadow of a once-human man emerges-forth, taking in his surroundings.

You stand in a vast chasm, as far and wide as the eye can see. A strong wind creates a persistent, deafening howl - engulfing you in a cloud of blistering dust. Within the cacophony, you hear the sound of clanging metal, thunderous roaring, and women screaming.

You are not alone.

Without warning, a man bursts forth, sword and shield in hand. Before speaking another word, he is cut down by a great bear, the size of a house. Tearing into the man’s neck with a splash of blood and gore - the man vanishes.

In his place - an armored hand bursts from the dirt. The man is immediately reborn.

However, the bear turns to you. 
    -> Section2

= Section2
* And that is the last you remember.

"Do not fret. You are safe. This was just a simulation, after all. Please allow me to release you." 

- With that, your restraints come undone. You stand, rubbing your wrists.

"You are free. Please return if you would like to explore any other nodes. Perhaps one without bears."

He laughs maniacally as the television shuts off.

* You do not understand.
-> END