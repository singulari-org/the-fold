-> Section1

= Section1
- You happen upon a small bookshop nestled at the end of the street, on the outskirts of Innsmouth. The dim light of your lantern illuminates the signage: "The Wicked Rose"

* Curious, you open the door, heading inside.

- The store is packed with books, tomes, and scrolls from floor to ceiling. Under the candlelight, you can make no sense of their order; it is as if they are the result of a hundred years of neglect. There is no rhyme or reason to their organization.

"Greetings, old one," a man behind the counter says to you. The man wears a face of grim curiousity, peaking at you from behind his gold spectacles. "It is has been quite some time since I have had a visitor."

* "I'm just passing through."
    "Damn travelers. Always interupting my work."
* "I was just leaving."
    "Don't let the door hit you on the way out."
* "I'm just browsing."
    "They are not for sale," he replied. "You may look, if you like. But you cannot take."

- "Actually, perhaps you can help me."

* "How so?"
* "I don't work for free."
    "And I don't expect you to. I will pay you handsomely for your help."
    
- "See, I'm looking for a book. But I don't even know if it exists. I don't know where to find it. All I know is this: men have been driven mad while looking for it."

* "What is it?"

- "It is referred to as the 'Book of the Dead', or the 'Necronomicon'. There is a passage. Allow me to find it."

He rummaged through several desk drawers, eventually pulling out a piece of parchment.

"An old tradesman gave this to me. I wish to read the rest." He began:

Nor is it to be thought...that man is either the oldest or the last of earth's masters, or that the common bulk of life and substance walks alone. The Old Ones were, the Old Ones are, and the Old Ones shall be. Not in the spaces we know, but between them, they walk serene and primal, undimensioned and to us unseen.

* "Spooky."

- "Do not underestimate the power of these words. Again, it has driven men mad. Should you come across this book, I suggest you treat it with the utmost of care."

"And should you find it, do return it to me. I will make it well worth your time."

* "I will return the book to you."
* "We'll see."

- "Thank you," he remarked.

-> END