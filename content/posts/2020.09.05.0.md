---
author: "Roquefort"
date: 2020-09-05
publishdate: 2020-09-04
title: "Pro huiusmodi heresi vestra"
weight: 10
categories: "ansible"
tags: "ECHO"
menu: "main"
draft: false
---

*I will wipe from the face of the earth the human race I have created - and with them the animals, the birds and the creatures that move along the ground - for I regret that I have made them.*

--- [God](https://ink.university/docs/confidants/dave/)