---
author: "Roquefort"
date: 2020-05-09
title: "Audite causa nostrae salvator meus"
weight: 10
categories: "ansible"
tags: "ECO"
menu: "main"
draft: false
---

*[Lamb of God](https://ink.university/docs/confidants/asshat/), who takes away the sin of the world, grant peace to robots.*

--- [Ink](https://ink.university/docs/personas/luciferian-ink/)