# Game Objects
## Overview
---
This section contains information about game objects that will ship with the game. This list is non-exhaustive, including only objects within the game's main hub, Arkham. All other objects are to be created by players.

## NPCs
---
### Nyarlathotep (Professor Clemmons)
Nyarlathotep is also known as the crawling chaos. It is an evil god that can shape-shift into over a thousand different forms. The character was first found in Lovecraft’s poem titled Nyarlathotep. It was published in 1920 and is part of the original Lovecraftian canon. This being also appeared in a few other stories published throughout the years. This beast is so scary that like the sight of a basilisk, one glance is enough to drive a man insane. When it assumes the form of a human, it turns into an Egyptian Pharaoh. Under the auspices of humanity, this sinister man reels in followers with his slick tongue and turn of a phrase.

### Margaret Clemmons
Margaret is the wife and assistant of Professor Clemmons. She has died, and been resurrected by the professor many times throughout history. This time, she was burned at the stake, and the professor is having difficulty finding her soul. He fears that she has gone hollow, and cannot return.

Margaret shows signs of psychopathic tendencies. She sees people as toys and gets great joy out of toying with them. She seems to think of people as some kind of pet, and doesn't seem to realize that they find her violent behavior perturbing.

Margaret maintains a garden full of body parts - heads, fingers, toes - and she is able to create new life from these parts. In fact, it was with this garden that the professor created The Fold.

## Enemies
---
### Deep One
[![Deep One](/static/lovecraft-deep-one.0.jpg)](/static/lovecraft-deep-one.0.jpg)

Humanoid in appearance, Deep Ones are primarily aquatic in nature although they are capable of surviving on land for an indefinite period of time. They are hairless, have a scaly fish-like skin, feathered gills on their necks or upper torsos, and are possessed of webbed hands and feet which facilitate travel beneath the waves. Their heads are said to resemble those of frogs or fish, and they sport large, unblinking eyes which give them excellent sight in the depths of the ocean. Deep Ones are functionally immortal, and will never die unless subject to violence or accident. In addition, for reasons unknown, some Deep Ones will continue to grow over the course of their lives and become creatures of truly gargantuan proportions.

### Dimensional Shambler
[![Dimensional Shambler](/static/lovecraft-dimensional-shambler.0.jpg)](/static/lovecraft-dimensional-shambler.0.jpg)

Hunched humanoid monstrosities with leathery grey skin, Shamblers possess yellow slit-like eyes and long fangs. They tend to adopt a hunched, semi-simian pose, with their overlong arms dragging along the ground; these arms are tipped with a pair of enormous and wickedly sharp claws. Although appearing to be corporeal, their dimension-hopping nature means that they possess no actual physical form on Earth, a fact which makes defeating a Shambler using conventional methods highly problematic, if not completely impossible.

### Formless Spawn
[![Formless Spawn](/static/lovecraft-formless-spawn.0.jpg)](/static/lovecraft-formless-spawn.0.jpg)

Somewhat similar to a Shoggoth, the Formless Spawn are amorphous creatures composed of an acidic tarry black substance which is extremely difficult to harm. Whilst in a state of rest, they appear as pools of black oil, but when roused to action are capable of assuming a variety of forms to suit their purpose; for example, they may extrude tentacles, form solid walls, or simply ooze between the cracks of a building to get to their prey.

### Ghast
[![Ghast](/static/lovecraft-ghast.0.jpg)](/static/lovecraft-ghast.0.jpg)

Hailing from Lovecraft’s surreal Dream Cycle, ghasts are humanoid creatures that live in the vaults of Zin. They have a vaguely human face, but lack noses or ears. They also have kangaroo-like legs which they use to hop around on and are very swift, strong, and agile. They hate sunlight and thus dwell in complete darkness, with sunlight capable of destroying them. Hunting in packs, they are fearsome hunters who will practice cannibalism if they get too hungry.

### Great Old One
[![Great Old One](/static/lovecraft-great-old-one.0.jpg)](/static/lovecraft-great-old-one.0.jpg)

Ancient, powerful creatures whose origins lie in the distant past of the cosmos, no two Great Old Ones are exactly alike, although they tend to share similar traits. Most are colossal monstrosities formed from the very fabric of the universe and virtually impossible to destroy, have minds whose thought processes are incomprehensible to humans, show little to no regard for the consequences their actions may have on lesser beings, and drive to insanity any who look upon their true form. In addition, the majority of them are able to influence the minds of sentient creatures even when sleeping or dormant. Initially believed to have been merely indifferent to the fates of those unfortunate enough to be around them, recent writings have shown the majority of them to be actively hostile to the mortal population of the cosmos.

### Hound of Tindalos
[![Hound of Tindalos](/static/lovecraft-hound.0.jpg)](/static/lovecraft-hound.0.jpg)

Although their exact appearance is somewhat vague, cosmic time-traveller Titus Crow described them as looking like bat-like rags, whilst other sources claim them to have long proboscises to drain their prey's bodily fluids, and that they constantly ooze a disgusting blue ichor. Despite being named the Hounds of Tindalos, this moniker refers more to the creatures' behaviour than any resemblance to an actual canid.

Originating in the Earth's distant past when other life had yet to develop past the single-called organism level, Hounds are hunters who crave an indescribable something in humankind, and once they pick up the scent of a man (usually the trail of a time-traveller) the beasts will follow their victims across time and space in order to feed. To this end, Hounds are capable of materialising through acute corners; this is due to the fact that while most common life inhabits the curves of time, Hounds inhabit the angles.

### Hunting Horror
[![Hunting Horror](/static/lovecraft-hunting-horror.0.jpg)](/static/lovecraft-hunting-horror.0.jpg)

Gigantic creatures resembling great annelids or serpents, the Hunting Horrors also have a huge pair of bat-or dragon-like wings of a rubbery texture, strangely distorted heads, and twisted appendages which end in wicked talons. It is said that their physical forms continually shift and change, making it difficult to focus on the creature as a whole. Serving Nyarlathotep in the role of attack dogs, the Horrors have a deep aversion to light in all its forms; indeed, it is possible to reduce one to ash with a strong enough burst of light.

### Mi-Go
[![Mi-Go](/static/lovecraft-mi-go.0.jpg)](/static/lovecraft-mi-go.0.jpg)

Aliens that looks like a cross between fungus and lobsters, mi-gos sure are strange. They fly through vacuum of space, zooming between Earth and Pluto with the aid of their supernatural wings. They worship other Lovecraft gods, acting as servants to them, being classified as a hostile and rather vicious alien species. In ancient times, they waged a war against the Elder Things on Earth before humans came into existence.

### Night-Gaunt
[![Night-Gaunt](/static/lovecraft-night-gaunt.0.jpg)](/static/lovecraft-night-gaunt.0.jpg)

Nightgaunts serve many gods and sometimes capture people climbing the mountains in the Dreamlands. They were inspired by nightmares Lovecraft had in his youth. Slithering through the Dreamlands, they collectively gave birth to the human conception of demons, with their long tails, closed feet, horns, and great bat-like wings. They’re said to have been inspired by nightmares Lovecraft himself suffered from.

### Shantak
[![Shantak](/static/lovecraft-shantak.0.jpg)](/static/lovecraft-shantak.0.jpg)

Enormous birds larger than any elephant, Shantaks have a slimy, reptilian skin, a pair of membranous, bat-like wings and a head which is said to resemble that of a horse. They are known to inhabit the Cold Wastes region of Earth's Dreamlands, where they are sometimes used as mounts by the denizens of that strange land, although they are deeply fearful of the Nightgaunts which also live there.

Many Shantaks are known to serve the Outer God Nyarlathotep, and many more can be found in the service of the Great Old One Ithaqua, but the Shantaks of the Dreamlands (amongst other creatures) are frequently ridden down and killed for sport by the Elder God Nodens, possibly explaining their fear of Nightgaunts; in the Dreamlands, Nightgaunts serve as the hunting-dogs of Nodens.

### Shoggoth
[![Shoggoth](/static/lovecraft-shoggoth.0.jpg)](/static/lovecraft-shoggoth.0.jpg)

The Shoggoths were created by the Elder Things as a slave race, taking the form of grotesque blobs covered in dozens of eyes. They have tremendous strength and are nearly invincible against forms of physical attack. Eventually, they developed a consciousness of their own and rebelled against the Elder Things, resulting in them roaming the dark spaces of the world in the modern day. Pray you don’t need one.

### Spider of Leng
[![Spider of Leng](/static/lovecraft-spider.0.jpg)](/static/lovecraft-spider.0.jpg)

Purple beasts that appear somewhat similar to Earth spiders yet on a much larger scale, the smaller of the Spiders of Leng are the size of a Shetland pony; larger specimens would tower over an elephant. They possess a degree of intelligence far higher than that of regular animals, but are not above the cannibalisation of other Leng Spiders. Whilst they are capable of spinning webs to disable their victims, they also possess a venomous bite of great potency, and have the ability to climb sheer surfaces, much like their counterparts in the waking world. 

## Equipment
---
The game should include a variety of weapons, armor, and items. The exact specifics are not important at this time.