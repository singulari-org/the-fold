# Monetization
## Overview
---
There are a number of successful, well-funded open source companies. To my knowledge, there are no large video game companies within this space.

This is because traditional video games are a product - not a service. Customers want to buy a functional video game - not fund the development of one.

We wish to build a video game - The Fold - as a service. Development will never stop, and features will be added so long as there continues to be customer demand for them. We wish to build a game that lasts for the long-term (20+ years).

## Examples
---
### Star Citizen
[Star Citizen](https://robertsspaceindustries.com/) is a game that has raised more than $212 million in crowdfunding. This proves that players will rally behind a game that they want to see. However, Star Citizen has been in development for more than 9 years, and is still far from a playable state. Many players believe that the game will never see a full release.

Development is slow because game development is closed-source, and centralized within Roberts Space Industries. It is expensive because it is a huge 3D game, which greatly increases the development burden. Had they chosen to open-source the project, and had they made it 2D, you would see much faster development.

### World of Warcraft
[World of Warcraft](https://worldofwarcraft.com/en-us/) has grossed more than $10 billion since its inception in 2004. They are largely based upon this subscription model, though they have similar problems to Star Citizen; development efforts are centralized, thus new releases are slow and incremental. 

### Rimworld
[Rimworld](https://rimworldgame.com/) is a highly successful video game made by a single, solitary developer. While the game suffers the same problems as the former two, it has an enormous [modding community](https://steamcommunity.com/app/294100/workshop/) - proving that developers want to contribute to games.

### Red Hat
The open source company, [Red Hat](https://www.redhat.com/en), was purchased by IBM in 2018 for $34 billion. While this was the largest purchase of this type, there are many other open source companies succeeding within this space.

### Gitlab
[Gitlab](https://about.gitlab.com/) - a software company - is valued at $2.75 billion. They follow a similar model to the one I propose; they run the company as one that provides a service. All of their products are open source.

## Plan
---
### Identify the customer base
Initially, we need to find out who our customers are. If nobody wants this game, development is pointless. 

We will find our customers by doing the following:

1) Publish this design document
2) Blitz social media
3) Draw paying customers to our Patreon page. We ask for only $1 per customer; it is not about the money. It is about the volume of customers. 

### Seek funding
Upon reaching 1000 Patreon subscribers, we will seek funding from outside investors. We will seek the following:

50% equity stake in our company in return for $1 million. We will use this to hire staff, funding a proof of concept which will be available after the period of 1 year. The other 50% equity will be split among the core staff. 

### Sell early access
After the first year, early builds will be made available to the public for free. The game is open source, after all, so it must be free to play.

We will have three avenues for potential revenue:

1) Players may purchase a multiplayer key for a fixed dollar amount
2) Players may pay a subscription fee on Patreon to fund the ongoing development of the Fold. They will also have direct influence of what is being developed, and how.
3) Players may purchase in-game premium assets.