# Setting
{{< hint warning >}}
**Warning**  
The following content is not final, and of poor quality. It is here to demonstrate the Ink engine - not our writing skills.
{{< /hint >}}

{{< ink file="Arkham.Introduction.js" >}}

## Other areas
---
- [Arkham Sanitarium](/docs/setting/Arkham.Sanitarium)
- [Innsmouth Thrift Shop](/docs/setting/Innsmouth.ThriftShop)
- [Innsmouth Book Store](/docs/setting/Innsmouth.BookStore)