# Reference
- [Bioshock Pitch](/static/BioshockPitch.pdf)
- [Detailed Game Design Document Template](/static/gddtemplate.pdf)
- [Blockchain-based Real-time Cheat Prevention and Robustness for Multi-player Online Games](/static/ibm-blockchain-anticheat.pdf)
- [A Decentralized Service Discovery Approach on Peer-to-Peer Network](/static/p2p-service-discovery.pdf)