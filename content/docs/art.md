# Art, Sound, and Music
*This section contains high-level information and examples about the visual and auditory style
for the game, as well as links to more specific information.
The high-level information here includes reference and concept art (and where possible, music)
to give the reader an idea of the visual style and tone for the game. Reference art may be taken
from movies, books, other games, etc., so long as it is not used in the game in any way. Concept
art (if any) must be original to the game. This art gives the reader a clear idea of the
environments, characters, opponents, objects, major locations, key events, user interface, etc.
in the game. A separated (linked) doc, usually in the form of a spreadsheet, delineates all known art and
sound assets that are needed for the game. This includes all user interface assets, animations,
environments, characters, etc, in complete detail.*

## Art
---
Art is to be left to the players, and the artists on-staff. The game will allow players to upload their own assets, and share them with the community. 

Here is one example of Andrew Saltmarsh's work:

[![Saltmarsh](/static/saltmarsh.0.jpg)](/static/saltmarsh.0.jpg)

## Sound and Music
---
Music shall be left to the musicians, for the most part. Examples of great video game music include:

- [Perturbator](https://www.youtube.com/watch?v=1Vsf3zYppP4)
- [DOOM Soundtrack](https://www.youtube.com/watch?v=Jm932Sqwf5E)
- [Swords and Sworcery Soundtrack](https://www.youtube.com/watch?v=xCcWWcL_Sg4)
- [Persona 5 Soundtrack](https://www.youtube.com/watch?v=WZ0p1eVm3FY)

Sounds will be left to the musicians, as well.