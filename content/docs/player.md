# Player
## Overview
---
The player is a patient of Arkham Sanitarium. After dying while inside of The Fold, he wakes up in a medical bed. He is being experimented on by the doctor's of the sanitarium, though not against his will; they allow him to be free if he so chooses.

He does, exploring the grounds and nearby cities. He even tests his luck in the forest - dying once again, only to be reborn in Margaret's garden.

After a while, he realizes that there is nowhere else to go. He is bored. He will explore The Fold further, to see if there is a way out of this place.

With the help of the doctors, he connects his mind to the machine, turning it into a single point of consciousness - a Spark - flying along synapses in The Fold. When landing upon a node, he manifests into a human. His goal is to capture other Sparks to fight alongside him in battle, to increase his power, and to contribute to the resources of his guild.

When the game starts, the player knows nothing. He learns through exploration.

## Goals
---
Initially, the player's primary goals are to explore the overworld map (known as "The Fold"), looking for dungeon nodes. The player should look for low-level, easy dungeons to do the following:

1. Capture 2 enemies to fight alongside the player.
2. Obtain equipment such as weapons, armor, and items.

Over time, the player will learn that they cannot progress into more difficult dungeons without assistance. They should do the following:

1. Seek to join a guild, where they can obtain a job and a source of income.
2. Seek to train skill(s) so that the player may increase their usefulness to the guild, thereby increasing income. 

With more resources, a player may seek to start their own guild, or perhaps just their own shop. Shops cost money to maintain, so success in business is paramount. They may also seek to capture nodes from other guilds.