# Contribution

## Overview
---
[Singulari.org](https://singulari.org) is actively seeking help from like-minded individuals. If you would like to contribute, please consider the following options:

### Development
#### Funding
Support development of The Fold. All proceeds go to families in-need.

##### Patreon
Show investors that you want to see this project succeed over at the [Official Patreon Page](https://www.patreon.com/fold). 

##### Bitcoin
Anonymously support our work with Bitcoin: bc1q5gkrsvdkmt6g5a3e357vkazz8mx8fjrcyup5ss

#### Chat
Join the discussion over at [our Official Discord Server](https://discord.gg/3w4UVMV).

#### Code
Contribute to the code, art, music and more over at [our Official Gitlab Repository](https://gitlab.com/singulari-org/the-fold).

### Message
If you have an idea, a question, or want to contribute differently, [please contact us via this form](https://share.hsforms.com/1_MJqyiB0SE2r-lOIw8cdKQ4kehm).