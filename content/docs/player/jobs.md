# Jobs
## Structure
### Guilds 
- Jobs are provided to players via Guilds or Shops
- Players are paid via other players
### Shops
- Run by players, must be staffed
### Caravans
- Traveling cities

## The Player
- Players are assigned to one job 
- Players may perform any job while logged-on
- While logged-off, players become an NPC that will work his/her primary job autonomously
### Builder
- The builder is a workhorse, assigned to hauling materials, building structures, and following orders
- A builder may act as an architect, when building in unclaimed land. 
### Inventor
- An inventor uses in-game editors, and real-world code to construct new blueprints
- An inventor is responsible for most of the assets in the game
### Engineer
- An engineer constructs items from blueprints. 
### Loremaster
- A loremaster is responsible for building the game world. He/she should be articulate, creative, and dedicated
- A loremaster must write tomes, detailing stories and legends.
- Tomes may be region-specific, or they may contain placeholders - such that they may be re-used across the world.
### Mystic
- A mystic dabbles in the arcane arts. He/she may create potions, author skillbooks, enchant items, or bind new souls to the world.
- A mystic may communicate with distant lands telepathically, or teleport there directly.
### Cleric
- A cleric is a low-magic individual, responsible for healing and prayer arts.
### Trader
- Responsible to establishing trade routes between cities, towns, and otherwise
- Traders should travel in groups, preferably with strong Mercenaries and Cartographers
- Trading is a great way to see the world
### Cartographer
- A cartographer is responsible for traveling the furthest reaches of the world
- While traveling, a cartographer is automatically building maps. Skill level determines how detailed these maps are.
- Combining different maps should result in newer, better maps. 
- Maps should be duplicated and sold to players
### Shopkeeper
- A shopkeeper is responsible for tending to stores, selling items to players, and sourcing new inventory.
### Mercenary
- A mercenary is a hired-arm, responsible for protecting a city, village, keep, or Caravan.
### Quartermaster
- A quartermaster builds weapons and armor
### Laborer
- A laborer is the most versatile of all roles. He may be responsible for working fields, harvesting crops, tending to livestock, fishing, lumberjacking, mining, digging, and more.
- Laborers are responsible for providing food to cities, upkeeping citizen's needs
### Criminal
- A criminal is an outlaw. 
- A criminal 
### Prisoner
- A prisoner is a captured criminal (or perhaps an innocent person)
- A prisoner may act as a Servant or Laborer
### Guildmaster
- The most prestigious of all roles. A guildmaster can perform any job, enact any rules, and start any war. 
- A guildmaster may self-elect
- A guild may be comprised of multiple guildmasters
- One should expect that most guilds will fail, for a lack of followers. 
- A guildmaster may be overthrown. If a guildmaster dies, or becomes a prisoner, the town will be left without leadership. New leadership may be established via traditional means.
### Architect
- The architect is a creative-type, permitted to construct blueprints of structures within a city, town, fortress, etc. 
- Blueprints are not immediately built. Work must be performed by Builders
### Noble
- A noble is a leader, responsible for building alliances, governing peoples, and establishing a strong economy.
- A noble may act in the absence of a guildmaster. He/she may hold town meetings, establish criminal trials, and sentence wrong-doers.
### General
- A general may lead armies of men into battle, either against neighboring factions or NPCs in dungeons.
### Citizen
- A citizen may be assigned to any job. Typically, these jobs will be low-risk, low-pay. (cooking, cleaning, etc)
### Rogue
- A spy may visit enemies under the guise of another role. He may hide his appearance from others.
- A spy may travel unseen, and quickly.
### Founder
- A founder is a paying-member of The Fold. 
- A founder is the holiest of all players, and receives blessing and incentives as a result.
- A founder receives a generous fixed-income