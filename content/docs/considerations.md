# Technical Considerations
## Technology
---
In The Fold, bleeding-edge technology is paramount. This is because we intend to build a game that lasts the test of time; we should not settle for old technology.

We intend to build a game that supports multiplayer, while requiring no server-side components. This will save us money, and save players the headache of needing to run a server.

### Game Engine
The Fold will use the [Godot](https://godotengine.org/) game engine. As opposed to more traditional game engines, like Unity or Unreal, Godot is free and open source. 

### Storytelling Engine
Narrative is written with [Ink](https://www.inklestudios.com/ink/), a storytelling tool. Examples of Ink's capabilities may be found under the [Setting](/docs/setting) page.

### Target Platform
Current platforms are PC, Mac, and Linux. All can be achieved with relative ease.

Mobile-friendly design should be a priority, since this will be on Android and iOS in future iterations.

### DNS
Blockchain-based DNS. IPNS or DNSLink.

### TempleOS
Completely contradictory to the above technologies, we are also considering the adoption of a brand new operating system for this project. More details about why [can be found here](https://ink.university/posts/journal/2020.05.31.0/).

## Multiplayer
---
### Sharing
Since there are no central servers to broker connections, players must have another way to share cities with others. We recommend using QR codes that work in peer-to-peer fashion, similar to how iCalendar links work.

### Chat
Again, because there are no central servers, chat must use a different medium. We recommend leveraging a blockchain chat app, such as the Ethereum-based [Status](https://github.com/status-im).

### Game State
Because central servers do not exist, game state must be persisted to disk, then asynchronously shared with peers. Because of the potential for local exploit, all operations must also be performed on peers. [Blockchain is the perfect medium for this.](/static/ibm-blockchain-anticheat.pdf). 

### Assets
Assets must be stored by using the InterPlanetary File System, [IPFS](https://ipfs.io/). This will allow us to save and share large files without the need for central storage, and without needing to save them within the git repository.

## World
---
### Procedural Generation
As a player explores outward from a city, additional nodes should spawn automatically. Such nodes should be procedurally-generated. Nodes should be deterministic; all players should generate the same nodes. This should be rather simple, given that nodes themselves are rather simple. Procedural generation will only really require the generation of a background, a foreground, battle maps, and/or creatures. 

*Technical documentation*
*• Major technical areas and risks*
*• Target hardware and specific requirements/assumptions*
*• Infrastructure*
*o directory structure and file naming conventions*
*o data file format*
*• Server, client, and network architecture (as needed)*
*• Artificial intelligence and other autonomous/procedural systems*
*o Procedural world creation*
*o Pathfinding*
*o Economy*
*• Technical (programming doc for each major feature or system, linked to the design doc*
*for each above. These docs are for programmers, and focus on implementation details*
*rather than the design itself or its rationale.*
*• Game cheats for testing*

*Current Target Platform (and any system requirements)*
*What is the first or current platform for the game – PC, web, tablet, phone, or something else?*
*Keep this to the current target platform; leave off any “future” desired platforms.*
*Competition*
*What games are similar from the player’s point of view, or would compete directly with your*
*game? Show their name, release date, any revenue and budget information you can find, and a*
*brief description of each. Also include a brief description of what sets your game apart.*
*Monetization*
*If the game is “free to play” or has in-game purchases, these need to be wholly integrated with*
*the rest of the design. This section provides an overview of the monetization philosophy and*
*the major systems. It is heavily linked with the systems and features section.*