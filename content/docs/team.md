# The Team
The Fold will require a number of core staff. The following have tentatively agreed to work on this project if funding is secured:

## Committed
---
### Game Design
- [The Architect](https://ink.university/docs/personas/the-architect)

### Writers
- [Luciferian Ink](https://ink.university/docs/personas/luciferian-ink)

### Artists
- [The Salt](https://ink.university/docs/confidants/artist/)
- [The Relaxer](https://ink.university/docs/confidants/er/)
- [The Children](https://ink.university/docs/confidants/the-children/)

### Actors
- [AK-47](https://ink.university/docs/confidants/mercenary/)
- [The Girl Next Door](https://ink.university/docs/confidants/fbi/)
- [The Interrogator](https://ink.university/docs/confidants/interrogator/)
- [The Inventor](https://ink.university/docs/confidants/inventor/)
- [The White Rabbit](https://ink.university/docs/confidants/white-rabbit/)

### Musicians
- [Caligula's Horse](https://www.youtube.com/watch?v=Ipi6UgznmlE&list=PL0fynal7pCth9AzKoSHT65_71UjlwHvEG)
- [Coheed and Cambria](https://www.youtube.com/watch?v=h4KwSuJfCjg&list=PLobf-n9Ax13YyTkl2up1j9NqZOfrgPSzT)
- [Ghost](https://www.youtube.com/watch?v=-0Ao4t_fe0I)
- [Neutral Milk Hotel](https://www.youtube.com/watch?v=YnTg35RDxdA)
- [Shawn James](https://www.youtube.com/watch?v=wvdUtaWBEYQ)
- [Toehider](https://www.youtube.com/watch?v=QSc3FOoecuM)
- [Tub Ring](https://www.youtube.com/watch?v=27OIaDSv6Es)

### Godot Developers
- [Game Development Center](https://www.youtube.com/c/GameDevelopmentCenter/featured)

### Engineers
- [The Avatar](https://ink.university/docs/confidants/avatar/)
- [The Thief](https://ink.university/docs/confidants/thief/)

## Targeted
---
### Blockchain Experts
- ???

### IPFS Experts
- ???

### C# Expert
- ???