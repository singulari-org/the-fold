# Character Creation
## Overview
---
Players should be able to style their own characters. Options should include:

- Gender
- Head
- Hair
- Torso
- Legs

## Data Structure
---
```
playerStyles:
  gender:
    - 000001
    - 000002
  head:
    - 000001
    - 000002

party:
  player:
    style:
      gender: 00002
      head: 00001
      hair: 00034
      torso: 000165
      legs: 000011
```