# Storytelling
## Overview
---
There should be a system in place for players to write stories, and assign them to NPCs and players. This system should leverage [Ink](https://www.inklestudios.com/ink/).