# Reputation
## Overview
---
Two systems for reputation should exist:

1. Inter-guild reputations (as a result of sieges, trade policy, caravans, etc)
2. Player-guild reputations (a player's standing with a specific guild)