# Blueprinting
## Overview
---
All equipment and items require their creator to have a blueprint for that item. This ensures that high-level items cannot be made by just anyone; they must be made by professionals - players who hold the correct blueprints. 

Blueprinting should be an industry. Creating copies of blueprints should be lucrative.

## Research Points
---
Each blueprint may be mastered by a player by a certain number of research points. Research points are directly proportional to time; there will be so many blueprints in the game, that it will be infeasible for a single person to learn them all. People must specialize, share, and work together.

## Prerequisites
Each item has a set of prerequisite items that must be researched in order to research the item. See the diagram below.

## UI
---
{{< svg file="/diagrams/Blueprinting.0.svg" >}}

## Data Structure
---
```
item:
  000250: 
    name: iron ore
    category: material
    label:
      - ore
  000300: 
    name: iron sword
    prereq: 
      - 000250
    category: weapon
    label:
      - sword
      - iron

party:
  player:
    blueprints:
      - 000250
      - 000300
```