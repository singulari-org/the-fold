## The Fold
---
The Fold is essentially an overworld map, comprised of nodes in a branching, tree-like structure. It should be reminiscent of neurons in the brain, or circuits in a computer.

In this image, the colors represent "territory" owned by different guilds. All nodes are connected to Arkham Sanitarium, since this is where the computer that facilitates rift-walking resides.

All nodes in this view are of the city or dungeon type.

The universe is populated by Nodes; or, activity hubs. Each node is connected to The Fold via a series of pathways, similar to (and modeled after) neural pathways connecting to synapses. The Fold represents the highest-level of gameplay within the game, including travel, trade, and exploration.

{{< svg file="/diagrams/TheFold.0.svg" >}}

Nodes represent the primary areas in which players will find themselves. There are two types of node: [Cities](/docs/setting/cities) and [Dungeons](/docs/setting/dungeons). 