# City Builder
Players with certain jobs (i.e. builders) should have access to a city-building toolset. This should allow them to build and expand upon cities.

## Visuals
---
A city node is to be comprised of just two visual components: the background, and the foreground. Each is interchangeable. For example:

### Background + Foreground
[![Stellaris](/static/stellaris.0.jpg)](/static/stellaris.0.jpg)

### Background + Different Foreground
[![Stellaris](/static/stellaris.1.jpg)](/static/stellaris.1.jpg)

## Construction
---
[Structures](/docs/systems/cities/structures)

## UI
---
A city node represents a main activity hub. This includes guild operations, trade management, crafting, jobs, and just about every other type of gameplay. 

[![Darkest Dungeon](/static/DarkestDungeon.0.jpg)](/static/DarkestDungeon.0.jpg)

## Data Structure
---
```
city:
  background:
    - 000001
    - 000002
  foreground:
    - 000001
    - 000002

node:
  type: city
  foreground: 000002
  background: 000001
```