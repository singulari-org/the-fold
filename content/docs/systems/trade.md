# Trade
## Overview
---
There should be a system in place to facilitate trading of resources automatically between guilds. Many resources will only be available to guilds via trade.