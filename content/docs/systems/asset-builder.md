# Asset Builder
## Overview
---
Players should have the ability to create custom assets - backgrounds, foregrounds, equipment, NPCs, and more - and upload them into the game for others to use. 


- There should be a mechanism in-place to phase-out/remove poor quality assets.
- Only specific job types may create assets. This ensures that guild masters can assign competent people to the task.
- Unused assets will be deleted after some period of time.
- Players may vote on assets. A high number of negative votes may result in the deletion of the asset.
- There should be a system for ordering/sorting/filtering assets