# Inventory
## Overview
---
Players should have a limited inventory for which they can hold items. They should have access to a much larger inventory within their houses.

## Data Structure
---
```
items:
  000001: 
    name: torch
    class: utility
  000450:
    name: copper sword
    class: weapon

party:
  player:
    inventory:
      000450: 3 # Count of item number 000450
      100345: 1
```