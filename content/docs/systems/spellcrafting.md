# Spellcrafting
## Overview
---
There should be a system in place for the crafting of new spells and scrolls. This system should take inspiration from Tyranny.

## Elements
---
Each spell has an elemental type. Enemies are weak to some elements, and resistant to others.

### Fire

### Water

### Electric

### Nature

### Life

### Death

### Chaos

### Order

## Effect
---
Each spell can be modified by the following:

### Straight Line

### Area of Effect

### Cone

### Touch

### Bounce

## Modifier
---

## UI
---
{{< svg file="/diagrams/Spellcrafting.0.svg" >}}

## Data Structure
---
```
Spells:
  Elements:
    212584:
      Name: Fire
    845125:
      Name: Water
    444444:
      Name: Water
  Effects:
    542458:
      Name: Touch
    888888:
      Name: Area of Effect
  Modifiers:
    878542:
      Name: Burn
  Constructed:
    000123:
      Name: Touch of Fire
      Prereq:
        Elements:
          - 212584
        Effects:
          - 542458
        Modifiers:
          - 878542

Party:
  Player:
    Spells: # Links to Spells:Constructed
      - 000123
```