# Economy
The economy will be the world’s first “truly” electronic currency. It will use blockchain, but hold no worldly value. Player devices will be mined while they are playing to generate currency that will be output through their assigned governments. Such money will be re-distributed to the people of those governments. Your government may pay monthly tribute to another government.

## Data Structure
---
```
guilds:
  000001: 
    name: The Flying Monkeys
    currency: 10030454
  000002:
    name: Arkham Custodians
    currency: 467

party:
  player:
    guild: 000002
    currency: 134
```