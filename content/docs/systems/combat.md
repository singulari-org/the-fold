# Combat
## Overview
---
Battles are a turn-based and party-driven. The battlefield is grid-based, from a 2D isometric perspective. The terrain may consist of many different things that provide advantages/disadvantages, such as plateaus, towers, buildings, rivers, trees, and more.

The player's party consists of 3 members: the player character, and 2 "captured" enemies. The enemy party may consist of more than 3.

Each player takes his/her turn in succession. One player's team goes first, then the second player's team goes next. 

The goal is to defeat the other player's team in battle.

## Action Points
---
Player characters have 2 action points per turn. These points may be spent in the following ways:

1. Move character
2. Move character

or;

1. Move character
2. Take action

or; 

1. Take action
2. Take action

## Cover
---

## Stealth
---

## UI
---
Dungeons represent areas where battles take place.

{{< svg file="/diagrams/Combat.0.svg" >}}

## Data Structure
---
```
npc:
  000350:
    name: Rock Troll
  000460:
    name: Mi-Go

party:
  player:
    health:
      total: 150
      current: 134
    current_ap: 2
    xCoords: 66
    yCoords: 45
  000350:
    health:
      total: 600
      current: 600
    current_ap: 0
    xCoords: 34
    yCoords: 42
  000460:
    health:
      total: 75
      current: 13
    current_ap: 1
    xCoords: 77
    yCoords: 56
```

## Similar titles
---
- [Xenonauts](https://www.youtube.com/watch?v=QWFYoP_0yH0)
- [XCOM 2](https://www.youtube.com/watch?v=DiVMpGPOGMc)
- [Divinity: Original Sin 2](https://www.youtube.com/watch?v=ga6uJaGyrEk)
- [Final Fantasy: Tactics](https://www.youtube.com/watch?v=ZLILfz5Hz0U)