# Traits
## Structure
- Traits increase with time working certain jobs
- Unused traits atrophy over time 
- Certain traits are completely lost by death (since the body is lost). Others may persist.
## Types 
### Strength
- Increases damage dealt by physical weapons
- Increases productivity as a laborer, builder, etc.
### Fearless
- Increases constitution in battle.
- Higher levels are immune to certain status effects
### Dexterity
- Increases productivity as a quartermaster, inventor, etc.
### Intelligence
- For magic
- Low intelligence has a direct impact on how a creature may be equipped. A lumbering troll, for example, may not be able to wear armor, or shoot guns.