# Quest
## Overview
---
There should be a system in place for the acceptance and tracking of quests given by NPCs and other players.

## Data Structure
---
```
quest:
  000001: 
    name: New Arrival
  000002:
    name: A Dark Secret
    prereq: 
      - 000001

party:
  player:
    quests:
      000001:
        acquired: True
        completed: False
```