# Caravan
## Overview
---
Offline players should be able to travel with a caravan to faraway lands. Travel is not immediate, thus, it would be helpful to allow travel to happen while players are away.

NPCs should make periodic visits via caravans, as well.