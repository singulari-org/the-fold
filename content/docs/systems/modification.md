# Modification
## Overview
---
Players should be able to purchase body modifications at Margaret's Garden. Such modifications should provide advantages as well as disadvantages.

## Data Structure
---
```
modifications:
  000001: 
    name: Bionic Arm
  000002:
    name: Bat Eye

party:
  player:
    modifications:
      - 000001
```