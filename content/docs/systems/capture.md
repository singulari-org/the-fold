# Capture
## Overview
---
After defeating enemies in battle, a player should be able to choose to "capture" the fallen NPC. The NPC will then fight for the player in future battles.

A player may hold up to 12 captured NPCs at any given time. However, they may only have 2 active in combat simultaneously. 

## Data Structure
---
```
npc:
  000350:
    name: Rock Troll
  000460:
    name: Mi-Go

party:
  player:
  000350:
  000460:
```