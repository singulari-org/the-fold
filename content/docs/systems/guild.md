# Guild
## Overview
---
The guild system should allow for the rulership over cities and territory.

- [Government](/docs/systems/guilds/government)

## Data Structure
---
```
guilds:
  000001: 
    name: The Flying Monkeys
  000002:
    name: Arkham Custodians

party:
  player:
    guild: 000002
```