---
headless: true
---

- [Overview]({{< relref "/" >}})
- [Blog]({{< relref "/posts" >}})
- ### Design
  - [Setting](/docs/setting)
  - [Player](/docs/player)
  - [Systems](/docs/systems)
  - [Game Objects]({{< relref "/docs/objects.md" >}})
  - [Art, Sound, and Music]({{< relref "/docs/art.md" >}})
  - [Technical Considerations]({{< relref "/docs/considerations.md" >}})
- [Team]({{< relref "/docs/team.md" >}})
- [Reference]({{< relref "/docs/reference" >}})
- [Contribute]({{< relref "/docs/contribution.md" >}})